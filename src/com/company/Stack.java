package com.company;

public class Stack<T> {
    private Node<T> head;
    private Node<T> tail;

    private class Node<T> {
        private Node<T> next;
        private T obj;

        public Node(T obj) {
            this.obj = obj;
        }
    }

    public void push(T obj) {
        if (head == null) {
            head = new Node<T>(obj);
            tail = head;
            tail.next = null;
        } else {
            Node<T> temp = new Node<>(obj);
            tail.next = temp;
            tail = temp;
            tail.next = null;
        }
    }

    public T pop() {
        if(head == null){
            throw new RuntimeException("Stack is Empty");
        }
        Node<T> temp;
        if (head.next == null) {
            temp = head;
            head = null;
        } else {
            Node<T> iterator = head;
            while (iterator.next != tail) {
                iterator = iterator.next;
            }
            temp = tail;
            tail = iterator;
            tail.next = null;
        }
        return temp.obj;
    }
}
