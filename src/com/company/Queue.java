package com.company;

import java.util.EmptyStackException;

public class Queue<T> {
    private Node<T> head;
    private Node<T> tail;

    private class Node<T> {
        private Node<T> next;
        private T obj;

        public Node(T obj) {
            this.obj = obj;
        }
    }

    public void push(T obj) {
        if (head == null) {
            head = new Node<T>(obj);
            tail = head;
        } else {
            Node<T> temp = new Node<>(obj);
            tail.next = temp;
            tail = temp;
            tail.next = null;
        }
    }

    public T pop() {
        if(head == null){
            throw new EmptyStackException();
        }
        Node<T> temp = null;
        if (head.next == null) {
            temp = head;
            head = null;
        } else {
            temp = head;
            head = head.next;

        }
        return temp.obj;
    }
}
