package com.company;

public class Tree {
    private  Node first;
    private class Node{
        Integer value;
        private  Node left;
        private  Node right;
        public Node(Integer obj) {
            this.value = obj;
        }
    }
    public void add(Integer obj){
       if (first == null){
           first = new Node(obj);
           first.left = null;
           first.right = null;
       } else{
           recursiveAdding(first, obj);
       }
    }
    private void recursiveAdding(Node obj, Integer value){
        if (value > obj.value){
            if (obj.right == null){
                Node o = new Node(value);
                obj.right = o;
                o.right = null;
                o.left = null;
            } else{
                 recursiveAdding(obj.right, value);
            }
        } else if (value < obj.value){
            if (obj.left == null){
                Node o = new Node(value);
                obj.left = o;
                o.right = null;
                o.left = null;
            } else{
                 recursiveAdding(obj.left, value);
            }
        }
    }
}
